#resource "vault_mount" "kv" {
#  path        = "k8s"
#  type        = "kv"
#  description = "kv secret engine managed by Terraform"
#
#  options ={
#    version = 2
#  }
#}

resource "vault_mount" "kv-reader" {
  path        = "reader"
  type        = "kv"
  description = "kv secret engine managed by Terraform"

  options ={
    version = 2
  }
}

resource "vault_mount" "kv-grafana" {
  path        = "mysql-myvm1"
  type        = "kv"
  description = "kv secret engine managed by Terraform"

  options ={
    version = 2
  }
}

resource "vault_mount" "kv-app-finance" {
  path = "app-finance"
  type = "kv"
  description = "kv secret engine managed by Terraform"

  options ={
    version = 2
  }
}

resource "vault_mount" "kv-elastic-credential" {
  path = "k8s/elastic"
  type = "kv"
  description = "kv secret engine managed by Terraform"

  options ={
    version = 2
  }
}

resource "vault_mount" "kv-taco-gathering" {
  path = "k8s/taco"
  type = "kv"
  description = "kv secret engine managed by Terraform"

  options ={
    version = 2
  }
}