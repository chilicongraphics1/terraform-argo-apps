locals {
  argocd_values = {
    environment = terraform.workspace
  }
}

resource "kubectl_manifest" "apps-reader-project" {
  yaml_body = file("${path.module}/resources/reader_appproject.yaml")
}

resource "kubectl_manifest" "apps-reader" {
  yaml_body = templatefile("${path.module}/resources/reader.yaml", {
    environment         = local.argocd_values.environment
  })
}