locals {
  argocd_values = {
    environment = terraform.workspace
  }
}

resource "kubernetes_namespace" "kubernetes-tastyexpedition-namespace" {
  metadata {
    name = "tastyexpedition"
  }
}

resource "kubernetes_namespace" "kubernetes-chilicongraphics-namespace" {
  metadata {
    name = "chilicongraphics"
  }
}

resource "vault_mount" "kv-chilicongraphics" {
  path        = "k8s/blog_chili"
  type        = "kv-v2"
  description = "kv secret engine managed by Terraform"
}

resource "vault_mount" "kv-tastyexpedition" {
  path        = "k8s/blog_tasty"
  type        = "kv-v2"
  description = "kv secret engine managed by Terraform"
}

resource "kubectl_manifest" "apps-blogs-project" {
  yaml_body = file("${path.module}/resources/sites_appproject.yaml")
}

resource "kubectl_manifest" "apps-blogs" {
  yaml_body = templatefile("${path.module}/resources/app_sites.yaml", {
    environment         = local.argocd_values.environment
  })
}