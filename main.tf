module "reader-app" {
  source  = "./modules/reader-app"
}

module "vaults" {
  source  = "./modules/key-vault"

  vault_token = var.vault_token
}