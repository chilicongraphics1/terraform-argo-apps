plan:
	terraform init
	terraform plan

apply:
	terraform init
	terraform apply -auto-approve

destroy:
	terraform init
	terraform destroy
